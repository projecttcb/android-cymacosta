package com.example.lectorcodigobarra;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;

import static android.Manifest.permission.CAMERA;
import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class MainActivity extends AppCompatActivity {

    private  Button btnScanner;
    private  TextView tvBarScanner;
    private  Button btnscanImg;
    private ImageView ImageViewScan;
    private  int boton = 0;
    private EditText editTextCode;
   // private EditText editTextCode;

    private final static int MY_REQUEST_CODE = 1;
    private ArrayList<String> permissionsToRequest;
    private ArrayList<String> permissionsRejected = new ArrayList<>();
    private ArrayList<String> permissions = new ArrayList<>();
    private final static int ALL_PERMISSIONS_RESULT = 107;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnScanner   = findViewById(R.id.btnScanner);
        tvBarScanner = findViewById(R.id.tvBarScanner);
        tvBarScanner.setMovementMethod(new ScrollingMovementMethod());
        btnscanImg   = findViewById(R.id.btnscanImg);
        ImageViewScan = findViewById(R.id.ImageViewScan);
        editTextCode = findViewById(R.id.editTextCode);

        btnScanner.setOnClickListener(mOnClickListener);
        btnscanImg.setOnClickListener(mOnClickListener);

        permissions.add(CAMERA);
        permissions.add(WRITE_EXTERNAL_STORAGE);
        permissions.add(READ_EXTERNAL_STORAGE);
        permissionsToRequest = findUnAskedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (permissionsToRequest.size() > 0)
                requestPermissions(permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

    }

    private ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<>();
        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }
        return result;
    }

    private boolean hasPermission(String permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Now user should be able to use camera
                } else {
                    // Your app will not have this permission. Turn off all functions
                    // that require this permission or it will force close like your
                    // original question
                }
                break;
            case ALL_PERMISSIONS_RESULT:
                for (String perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale(permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            requestPermissions(permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                        }
                                    });
                            return;
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("Aceptar", okListener)
                .setNegativeButton("Cancelar", null)
                .create()
                .show();
    }

    private void galleryIntent()
    {
        boton = 1;
        editTextCode.setEnabled(true);
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 100);
    }


  /*  private static String readQR(String pathname) throws FormatException, ChecksumException, NotFoundException, IOException {

        InputStream qrInputStream = new FileInputStream(pathname);
        BufferedImage qrBufferedImage = ImageIO.read(qrInputStream);

        LuminanceSource source = new BufferedImageLuminanceSource(qrBufferedImage);
        BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));
        Reader reader = new MultiFormatReader();
        Result stringBarCode = reader.decode(bitmap);

        return stringBarCode.getText();
    } */



    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.btnScanner:
                    //new IntentIntegrator(MainActivity.this).initiateScan();
                    editTextCode.setEnabled(false);
                    IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
                    integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                    integrator.setPrompt("Coloque el código de barras dentro del rectángulo");
                    integrator.setCameraId(0);
                    integrator.setOrientationLocked(true);
                    integrator.setBeepEnabled(true);
                    integrator.setBarcodeImageEnabled(true);
                    integrator.initiateScan();

                    break;
                case R.id.btnscanImg:
                    galleryIntent();
                    break;
                case R.id.editTextCode:
                    galleryIntent();
                    break;
            }
        }

    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        switch (boton){
            case 0:

                if( result != null ) {
                    if (result.getContents() == null){
                        //tvBarScanner.setText("Error al escanear el código de barra");
                        editTextCode.setText("Error al escanear el código de barra");

                    }else{

                        //editTextCode.setText("El código de barra es:\n" + result.getContents());
                        editTextCode.setText(result.getContents());
                        String urlimg = result.getBarcodeImagePath();
                        ImageViewScan.setImageBitmap(BitmapFactory.decodeFile(urlimg));
                    }
                } else {
                    super.onActivityResult(requestCode, resultCode, data);

                }
                break;
            case 1:
                editTextCode.setText("");
                //String urlimg = null;
                //ImageViewScan.setImageBitmap(BitmapFactory.decodeFile(urlimg));
                if (data != null) {
                    Uri imageUri  = data.getData();
                    ImageViewScan.setImageURI(imageUri);
                    if( result != null ) {
                        if (result.getContents() != null){
                            editTextCode.setText(result.getContents());
                            String urlimg = result.getBarcodeImagePath();
                            ImageViewScan.setImageBitmap(BitmapFactory.decodeFile(urlimg));
                        }else{
                            editTextCode.setText("Error al escanear el código de barra");
                        }
                    }
                }else{
                    ImageViewScan.setImageURI(null);
                }
                break;
            default:
                break;
        }


    }



}